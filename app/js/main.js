'use strict';

var app = {

      init: function () {

        app.header.open_menu();
        app.slider.init();
        app.form.init();
        app.accordion.init();
        app.projects.init();

        $(window).on('resize', function (e) {

          var window = $(this);


        }).trigger('resize');

      },

      header: {

        open_menu: function () {

          $(document).on('click', function (event) {

            var target = $(event.target);
            var toggle = $('.toggle-menu');
            var menu = $('.header__menu');
            var body = $('body');
            var overlay = $('.overlay');

            if (!target.closest('.header__inner').length && toggle.hasClass('active')) {
              toggle.removeClass('active');
              menu.removeClass('open');
              body.css('overflow', '');
              overlay.removeClass('active');
              return;
            } else if (target.closest('.toggle-menu').length && !toggle.hasClass('active')) {
              toggle.addClass('active');
              menu.addClass('open');
              body.css('overflow', 'hidden');
              overlay.addClass('active');
            }

          });

        }

      },

      slider: {

        init: function () {

          app.slider.images();
          app.slider.footer_blog();

        },

        images: function () {

          $('.slider').each(function () {

            var $slider = $(this);

            $slider.on('init', function (slick, direction) {

              $(window).on('resize', function () {
                app.slider.arrow_offset(direction);
              }).trigger('resize');

            });

            $slider.slick({
              infinite: true,
              slidesToShow: 3,
              slidesToScroll: 1,
              responsive: [
                {
                  breakpoint: 992,
                  settings: {
                    slidesToShow: 2
                  }
                }, {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1
                  }
                }
              ]
            });

            function arrow_offset(direction) {

              var prev = direction.$prevArrow;
              var next = direction.$nextArrow;
              var slider = direction.$slider;
              var img = slider.find('img');
              var offset = img.height() / 2;

              prev.css('bottom', offset);
              next.css('bottom', offset);

            }

          });

        },

        footer_blog: function () {

          var $slider = $('#footer-slider');

            $slider.on('init', function (slick, direction) {

              $(window).on('resize', function () {
                arrow_offset(direction);
              }).trigger('resize');

            });

            $slider.slick({
              infinite: true,
              slidesToShow: 2,
              slidesToScroll: 1,
              responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 1
                  }
                }
              ]
            });

            function arrow_offset(direction) {

              var prev = direction.$prevArrow;
              var next = direction.$nextArrow;
              var slider = direction.$slider;
              var img = slider.find('img');
              var offset = img.height() / 2;

              prev.css('top', offset);
              next.css('top', offset);

            }

        },

        arrow_offset: function (direction) {

          var prev = direction.$prevArrow;
          var next = direction.$nextArrow;
          var slider = direction.$slider;
          var img = slider.find('img');
          var offset = img.height() / 2;

          prev.css('bottom', offset);
          next.css('bottom', offset);

        }

      },

      form: {

        init: function () {

          app.form.file_input();

        },

        file_input: function () {

          var inputs = document.querySelectorAll('.form__file');

          for (var i = 0, len = inputs.length; i < len; i++) {
            customInput(inputs[i])
          }

          function customInput (el) {
            const fileInput = el.querySelector('[type="file"]');
            const label = el.querySelector('[data-js-label]');
            const button = el.querySelector('.button');

            fileInput.onchange =
              fileInput.onmouseout = function () {

              var value;

                if (!fileInput.value) {
                   value = 'Прикрепите подробное ТЗ';
                } else {
                   value = fileInput.value.replace(/^.*[\\\/]/, '');
                }

                button.innerText = value

              }

          }

        }

      },

      accordion: {

        init: function () {

          app.accordion.down();

        },

        down: function () {

          $('.accordion').on('click', '.accordion__title', function (e) {

            var $this = $(this);
            var $this_item = $this.closest('.accordion__item');
            var $this_content = $this_item.find('.accordion__content');

            if ( $this.hasClass('active') ) {

              $this.removeClass('active');
              $this_content.slideUp();

            } else {
              $('.accordion__title').removeClass('active');
              $('.accordion__content').slideUp('active');
              $this.addClass('active');
              $this_content.slideDown();
            }

          });

        }

      },

      projects: {

        init: function () {

          app.projects.slider();
          app.projects.accordion();
          app.projects.tabs();

        },

        slider: function () {

          var win = $(window);
          var projects = $('.projects__list');

          $(window).on('resize', function () {

            if (win.width() <= 768 ) {

              projects.each(function () {

                var $slider = $(this);

                if ( $slider.hasClass('slick-initialized') ) return;

                $slider.on('init', function (slick, direction) {
                  setTimeout(function () {

                    app.slider.arrow_offset(direction);

                  }, 300);
                });

                $slider.slick({
                  slidesToShow: 1,
                  slidesToScroll: 1
                });

              });

            } else {

              projects.each(function () {

                var $slider = $(this);

                if ( !$slider.hasClass('slick-initialized') ) return;

                $slider.slick('unslick');

              });

            }

          }).trigger('resize');



        },

        accordion: function () {

          $('.projects-content__title').on('click', function () {

            var $this = $(this);
            var $this_item = $this.closest('.projects-content__item');
            var $this_content = $this_item.find('.projects-content__inner');
            var $this_slider = $this_content.find('.slick-slider');

            if ( $this.hasClass('active') ) {

              $this.removeClass('active');
              $this_content.slideUp();

            } else {
              $('.projects-content__title').removeClass('active');
              $('.projects-content__inner').slideUp('active');
              $this.addClass('active');
              $this_content.slideDown();


              setTimeout(function () {
                $this_slider.slick('refresh');
              }, 0);

            }

          });

        },

        tabs: function () {

          var projects = $('.projects-tabs');
          var cont = projects.find('.projects-content');
          var nav = projects.find('.projects-nav');
          var nav_item = nav.find('.projects-nav__item');
          var cont_item = cont.find('.projects-content__item');

          nav_item.eq(0).addClass('active');
          cont_item.eq(0).addClass('active show');

          nav_item.on('click', function () {

            var $this = $(this);
            var i = $this.index();
            var $this_item = cont_item.eq(i);

            nav_item.removeClass('active');
            cont_item.removeClass('active show');

            $this.addClass('active');
            $this_item.addClass('active');
            setTimeout(function () {
              $this_item.addClass('show');
            }, 10);

          });

        }

      }

    }
;

$(document).ready(app.init());


























